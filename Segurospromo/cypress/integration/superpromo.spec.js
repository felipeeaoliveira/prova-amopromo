/// <reference types="cypress" />

describe('Tests', () => {
    it('deve selecionar ultimo pedido', () => {
        cy.visit('homolog1.segurospromo.com.br/site/pesquisa/brasil/2023-02-16/2023-02-22/');
        cy.get('[data-cy=botao-selecionar-seguro-p31]').click();
     

        });

        it('deve selecionar a opcao de cartao de credito', () => {
            
            cy.get('.continue > .optionone > .button').click();
            cy.get('#cartao-credito').click();
            cy.get('#cc-number').type('1111 111111 11111');
            cy.get('#cc-holder-name').type('Felipe Teste');
            cy.get('#cc-holder-cpf').type('75005808191');
            
            
            });

        it('deve aplicar cupom de desconto', () => {

            cy.get('#cupom').type('AMOPROMO');
            cy.get('.cupom-desconto > button').click();
                
            });    
    });